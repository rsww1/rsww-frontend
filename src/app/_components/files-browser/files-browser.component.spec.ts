import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilesBrowserComponent } from './files-browser.component';

describe('FilesBrowserComponent', () => {
  let component: FilesBrowserComponent;
  let fixture: ComponentFixture<FilesBrowserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilesBrowserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilesBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
