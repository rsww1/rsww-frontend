import {Component, OnInit, ViewChild} from '@angular/core';
import {FileService} from '@app/_services/file.service';
import {ProcessFile} from '@app/_models/ProcessFile';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {saveAs} from 'file-saver';

@Component({
  selector: 'app-files-browser',
  templateUrl: './files-browser.component.html',
  styleUrls: ['./files-browser.component.less']
})
export class FilesBrowserComponent implements OnInit {
  files: ProcessFile[];
  columns: string[] = ['id', 'file name', 'status', 'date', 'processButton', 'reportButton'];
  source: MatTableDataSource<ProcessFile>;
  colors = {waiting: 'red', running: 'blue', completed: 'green'};

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private fileService: FileService) {
  }

  ngOnInit(): void {
    this.fileService.getAllFiles().subscribe(processFiles => {
      this.files = processFiles;
      this.source = new MatTableDataSource(this.files);
      console.log(this.source);
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.source.filter = filterValue.trim().toLowerCase();

    if (this.source.paginator) {
      this.source.paginator.firstPage();
    }
  }

  sendToAnalyse(fileId: string) {
    console.log('file id:' + fileId);
    this.fileService.processFile(fileId).subscribe(taskId => {
      console.log(taskId);
    });
  }

  getReport(fileId: string) {
    this.fileService.getReport(fileId).subscribe(data => {
      const blob = new Blob([data], {type: 'application/pdf'});
      console.log(blob);
      saveAs(blob, 'report.pdf');
    });
  }

  resolveColor(status: string) {
    return this.colors[status];
  }

}
