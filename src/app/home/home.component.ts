﻿import {Component} from '@angular/core';

import {User} from '@app/_models';
import {AccountService} from '@app/_services';
import {FileService} from '@app/_services/file.service';
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';

@Component({templateUrl: 'home.component.html'})
export class HomeComponent {
  user: User;
  showAlert = false;
  alertType = 'danger';
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  isTemplateFile = false;

  constructor(private accountService: AccountService, private fileService: FileService, private snackBar: MatSnackBar) {
    this.user = this.accountService.userValue;
  }

  upload(event) {
    const file = event.target.files[0] as File;
    if (this.validateFile(file)) {
      this.showAlert = false;
      this.successNotify();
      if (this.isTemplateFile) {
        console.log('xddddddd');
      } else {
        this.fileService.uploadFile(file).subscribe(success => {
          console.log(success);
        });
      }
    } else {
      this.showAlert = true;
    }
  }

  private validateFile(file: File): boolean {
    const acceptedExtensions = ['.cs', 'jpg'];
    const extension = file.name.split('.').pop();
    return acceptedExtensions.indexOf(extension) > -1;
  }

  private successNotify(): void {
    this.snackBar.open('Successfully send file', 'OK', {
      duration: 1000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  close(): void {
    this.showAlert = false;
  }
}
