import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '@environments/environment';
import {ProcessFile} from '@app/_models/ProcessFile';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  private uploadFileUrl = '/api/file/save';
  private uploadTemplateFileUrl = '/api/file/template/save';
  private getAllFilesUrl = '/api/file/all';
  private processFileUrl = '/api/process/start';
  private reportUrl = '/api/report';

  constructor(private http: HttpClient) {
  }

  public uploadFile(file: File): Observable<boolean> {
    const formData = this.createFormData(file);
    console.log(`${environment.commandApiUrl}${this.uploadFileUrl}`);
    return this.http.post<boolean>(`${environment.commandApiUrl}${this.uploadFileUrl}`, formData);
  }

  public uploadTemplateFile(file: File): Observable<boolean> {
    const formData = this.createFormData(file);
    return this.http.post<boolean>(`${environment.commandApiUrl}${this.uploadTemplateFileUrl}`, formData);
  }

  public getAllFiles(): Observable<ProcessFile[]> {
    return this.http.get<ProcessFile[]>(`${environment.queryApiUrl}${this.getAllFilesUrl}`);
  }

  public processFile(fileId: string): Observable<string> {
    const param = new HttpParams().set('fileId', fileId);
    return this.http.post<string>(`${environment.commandApiUrl}${this.processFileUrl}`, param);
  }

  public getReport(fileId: string) {
    const params = new HttpParams().append('fileId', fileId);
    const headers = new HttpHeaders().set('Accept', 'application/pdf');
    return this.http.get(`${environment.queryApiUrl}${this.reportUrl}`, {headers, params, responseType: 'blob'});
  }

  private createFormData(file: File) {
    const formData = new FormData();
    formData.append('file', file, file.name);
    return formData;
  }
}
