﻿import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {environment} from '@environments/environment';
import {User} from '@app/_models';
import jwt_decode from 'jwt-decode';

@Injectable({providedIn: 'root'})
export class AccountService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;
  private loginUrl = '/api/user/login';
  private registerUrl = '/api/user/create';

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(login, password) {
    return this.http.post(`${environment.loginApiUrl}${this.loginUrl}`, {login, password}, { responseType: 'text' })
      .pipe(map(token => {
        const decoded = jwt_decode(token);
        console.log(decoded);
        const user = new User();
        // @ts-ignore
        user.id = decoded.sub;
        user.token = token;
        // @ts-ignore
        user.username = decoded.login;
        localStorage.setItem('user', JSON.stringify(user));
        this.userSubject.next(user);
        return token;
      }));
  }

  logout() {
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/account/login']);
  }

  register(user: User) {
    return this.http.post(`${environment.loginApiUrl}${this.registerUrl}`, user);
  }
}
