export class ProcessFile {
  id: string;
  userId: string;
  fileName: string;
  status: string;
  uploadDate: Date;
}
